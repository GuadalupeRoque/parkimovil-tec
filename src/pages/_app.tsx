import React, { ReactElement } from 'react';
import { AppProps } from 'next/app';
import '../../public/css/parkimovil.styles.css';

function App({ Component, pageProps }: AppProps): ReactElement<any, any> | null {
    return <Component {...pageProps} />;
}

export default App;
