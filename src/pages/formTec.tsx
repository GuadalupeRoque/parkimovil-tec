import Head from 'next/head'
import React, { useState, useEffect, Component } from 'react'
import {
  Field,
  FieldProps,
  Form,
  Formik,
  FormikProps
} from 'formik'
import Swal from 'sweetalert2'
import * as Yup from 'yup';
import { postUserTec, verifToken } from '../api/User'
import areaCodes from '../intl/area_codes';
import Router from 'next/router';



// Page props
type formTecCRUDProps = {
  student?: {
    email: string,
    firstName: string,
    lastName: string,
    phoneNumber: string,
    registration: string,
    areaCodes: string
  }
}

interface IDialogFormValues {
  email: string,
  firstName: string,
  lastName: string,
  phoneNumber: string,
  registration: string,
  areaCodes: string
}


export function formTec(props: formTecCRUDProps) {

  const initalValues = {
    email: "",
    firstName: "",
    lastName: "",
    phoneNumber: "",
    registration: "",
    areaCodes: areaCodes.areaCodes[142].dial_code
  };
  const [formData, setFormData] = useState(props.student);
  const [token, setToken] = useState(true);

  const phoneRegExp = /^[1-9]+[0-9]*$/

  const AccountSchema = Yup.object({
    email: Yup.string().email('Ingrese una dirección de correo.').required("Campo requerido."),
    firstName: Yup.string()
      .required("Campo requerido."),
    lastName: Yup.string()
      .required("Campo requerido."),
    phoneNumber: Yup.string().matches(phoneRegExp, 'Campo no válido')
    .length(10, "El campo es de 10 dígitos")
      .required("Campo requerido."),
    registration: Yup.string()
      .required("Campo requerido.")
  });

  useEffect(() => {
    getToken();
  }, []);

  function getToken() {
    const tokenValid = Router.query.token;
    verifToken(tokenValid).then(result => {
      setFormData(
        {
          email: result.data.email,
          firstName: result.data.firstName,
          lastName: result.data.lastName,
          phoneNumber: "",
          registration: result.data.registration,
          areaCodes: areaCodes.areaCodes[142].dial_code
        }
      ),
        setToken(true)
    }).catch(error => {
      console.log(error),
        setToken(false)
    })
  }

  function insertForm(
    values: IDialogFormValues, resetForm: any
  ) {
    console.log(values);
    postUserTec(values.email, values.firstName, values.lastName, values.phoneNumber, values.registration, values.areaCodes)
      .then(result => {
        resetForm({})
        Swal.fire(
          'Información enviada',
          '',
          'success'
        ).then(function () {
          window.location.href = "https://parkimovil.com";
        });
      })
      .catch(error => {
        if (error.response) {
          const codeError = error.response.data.error.code;
          errorCode(codeError);
        }
        else {
          Swal.fire(
            'Error de servidor',
            error,
            'error'
          )
          console.log(error);
        }
      });
  }

  function errorCode(codeError: number) {
    switch (codeError) {
      case 2015:
        Swal.fire(
          'Matrícula invalida',
          '',
          'error'
        )
        break
      case 2011:
        Swal.fire(
          'La cuenta Parkimovil no existe.',
          '',
          'error'
        )
        break
      case 2012:
        Swal.fire(
          'El número de teléfono ya está vinculado a otra cuenta Parkimovil.',
          '',
          'error'
        )
        break
      case 1001:
        Swal.fire(
          'El campo número telefónico debe ser mayor a 7 dígitos',
          '',
          'error'
        )
        break
      default:
        Swal.fire(
          'Error de servidor',
          '',
          'error'
        )
        break
    }
  };
  return (

    <React.Fragment>
      <Head>
        <title>Formulario Registro TEC Santa Fe</title>
      </Head>
      <div className="container  py-8 font-sans lg:container lg:mx-auto space-y-3 w-auto" >
        <div className="pl-8">
          <div className="mb-8">
            <img className="w-64" src="/img/parkimovil-logo-h.png" />
          </div>
        </div>
        <section >
          <div className="flex flex-wrap box-border bg-blue">
            <div className="w-1/2  text-white px-16 text-2xl mt-8 mb-8">
              <p >
                Tecnológico de Monterrey
          </p>
              <p className=" font-bold">
                Santa Fe
          </p>
            </div>
            <div className="w-1/2 bg-cover bg-center" style={{
              backgroundImage: "url('/img/campus-santa-fe-3.jpg')"
            }}>

            </div>
          </div>
        </section>

        {!token ? (
          <div className=" bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative" role="alert">
            <span className="block sm:inline">Al parecer el link ya ha caducado, o ha sido usado.</span>
          </div>
        ) : null}

        <div className="text-center">
          <h1 className="text-3xl font-semibold ">
            Registro de estacionamiento con Parkimóvil
          </h1>
        </div>
        {formData &&
          <Formik
            initialValues={formData}
            validationSchema={AccountSchema}
            onSubmit={(values, { setSubmitting, resetForm }) => {
              setSubmitting(false);
              insertForm(values, resetForm);
            }}
            render={(formikBag: FormikProps<IDialogFormValues>) => (
              <Form className="w-full ">
                <div className="text-center">
                  <p>
                    Favor de llenar el siguiente formulario con los datos correspondientes.
          </p>
                  <p>El número telefónico debe ser con el que se registró en la aplicación. </p>
                </div>
                <div className="flex flex-wrap -mx-3 mb-6">
                  <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                    <label className="block text-gray-700 text-sm font-bold mb-2" >
                      Nombre
                  </label>
                    <Field
                      id="firstName"
                      name="firstName"
                      render={({
                        field,
                        form
                      }: FieldProps<any>) => (<div>
                        <input className="bg-gray-200 shadow appearance-none border rounded w-full py-2 px-3 text-gray-500 leading-tight focus:outline-none focus:shadow-outline" id="name" type="text" {...field} disabled></input>
                        {form.errors.firstName && form.touched.firstName ? (
                          <div><p className="text-red-500 text-xs italic">{form.errors.firstName}</p></div>
                        ) : null}
                      </div>
                        )}
                    />
                  </div>
                  <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                    <label className="block text-gray-700 text-sm font-bold mb-2" >
                      Apellido
                  </label>
                    <Field
                      id="lastName"
                      name="lastName"
                      render={({
                        field,
                        form
                      }: FieldProps<any>) => (<div>
                        <input className="bg-gray-200 shadow appearance-none border rounded w-full py-2 px-3 text-gray-500 leading-tight focus:outline-none focus:shadow-outline" id="name" type="text" {...field} disabled></input>
                        {form.errors.lastName && form.touched.lastName ? (
                          <div><p className="text-red-500 text-xs italic">{form.errors.lastName}</p></div>
                        ) : null}
                      </div>
                        )}
                    />
                  </div>
                </div>
                <div className="flex flex-wrap -mx-3 mb-6">
                  <div className="w-full px-3">
                    <label className="block text-gray-700 text-sm font-bold mb-2" >
                      Correo electrónico
                  </label>
                    <Field
                      id="email"
                      name="email"
                      render={({
                        field,
                        form,
                      }: FieldProps<any>) => (
                          <div>
                            <input className="bg-gray-200 shadow appearance-none border rounded w-full py-2 px-3 text-gray-500 leading-tight focus:outline-none focus:shadow-outline" id="email" type="email" {...field} disabled></input>
                            {form.errors.email && form.touched.email ? (
                              <div><p className="text-red-500 text-xs italic">{form.errors.email}</p></div>
                            ) : null}
                          </div>
                        )}
                    />
                  </div>
                </div>

                <div className="flex flex-wrap -mx-3 mb-2">
                  <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                    <label className="block text-gray-700 text-sm font-bold mb-2" >
                      País
                  </label>
                    <Field
                      id="areaCodes"
                      name="areaCodes"
                      render={({
                        field,
                        form
                      }: FieldProps<any>) => (
                          <div className="relative">
                            <select className=" appearance-none w-full bg-gray-200 text-gray-700 border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline" {...field}>
                              {areaCodes.areaCodes.map((item, index) => (
                                <option key={index} value={item.dial_code}>
                                  {item.name}
                                </option>
                              ))}
                            </select>
                            <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                              <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" /></svg>
                            </div>
                          </div>
                        )}
                    />
                  </div>

                  <div className="w-full md:w-2/3 px-3 mb-6 md:mb-0">
                    <label className="block text-gray-700 text-sm font-bold mb-2" >
                      Número telefónico
                  </label>
                    <Field
                      id="phoneNumber"
                      name="phoneNumber"
                      render={({
                        field,
                        form
                      }: FieldProps<any>) => (
                          <div>
                            <input className=" bg-gray-200 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="phoneNumber" type="text" {...field} ></input>
                            {form.errors.phoneNumber && form.touched.phoneNumber ? (
                              <div><p className="text-red-500 text-xs italic">{form.errors.phoneNumber}</p></div>
                            ) : null}
                          </div>
                        )}
                    />
                  </div>
                </div>
                <div className="flex flex-wrap -mx-3 mb-6">
                  <div className="w-full px-3">
                    <label className="block text-gray-700 text-sm font-bold mb-2" >
                      Matricula
                  </label>
                    <Field
                      id="registration"
                      name="registration"
                      render={({
                        field,
                        form
                      }: FieldProps<any>) => (
                          <div>
                            <input className="bg-gray-200 shadow appearance-none border rounded w-full py-2 px-3 text-gray-500 leading-tight focus:outline-none focus:shadow-outline uppercase" id="registration" type="text" {...field} disabled ></input>
                            {form.errors.registration && form.touched.registration ? (
                              <div><p className="text-red-500 text-xs italic">{form.errors.registration}</p></div>
                            ) : null}
                          </div>

                        )}
                    />
                  </div>
                </div>

                <div className="text-center">
                  <button className=" bg-orange text-white font-bold py-2 px-4 rounded  " type="submit">
                    Guardar  </button>
                </div>
              </Form>
            )}
          />}
        <div className="text-center text-sm pt-8">
          <p >
            ©2020 Parkimóvil | <a href="https://parkimovil.com/aviso-de-privacidad.html">Aviso de privacidad</a>  |
         <a href="https://parkimovil.com/terminos-y-condiciones.html">Términos y condiciones</a>  </p>
        </div>
      </div>
    </React.Fragment>
  )
}
export default formTec;