import React from 'react';

const links = [
    { key: '', href: 'https://zeit.co/now', label: 'ZEIT' },
    { key: '', href: 'https://github.com/zeit/next.js', label: 'GitHub' },
].map((link) => {
    link.key = `nav-link-${link.href}-${link.label}`;
    return link;
});

const Nav = (): JSX.Element => (
    <nav>
        <ul>
            <li>
                <a href="/">
                    <a>Home</a>
                </a>
            </li>
            {links.map(({ key, href, label }) => (
                <li key={key}>
                    <a href={href}>{label}</a>
                </li>
            ))}
        </ul>
    </nav>
);

export default Nav;
