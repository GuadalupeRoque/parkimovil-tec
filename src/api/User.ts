import axios, { AxiosPromise } from 'axios'

export const postUserTec =
  (email: string, firstName: string, lastName: string, phoneNumber: string, registration: string, areaCodes: string): AxiosPromise<void> => {
    return axios.post(`/api/user/userTec`, { email, firstName, lastName, phoneNumber, registration, areaCodes })
  }

export const verifToken = async (token: string | string[]): Promise<any> => {
  const rs = await axios.get(`/api/user/verifToken?token=${token}`, { withCredentials: true })
  return {
    data: rs.data
  }
}
