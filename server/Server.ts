import dotenv from 'dotenv';
import { default as express } from 'express';
import expressWinston from 'express-winston';
import helmet from 'helmet';
import Knex from 'knex';
import nextjs from 'next';
import noCache from 'nocache';
import { Model } from 'objection';
import Logging from './Logger';
import configPageRouting from './PageRouter'
import configApiRouting from './api/ApiRouter'

dotenv.config();

// Server config
const port = process.env.PORT || 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = nextjs({ dev });
const handle = app.getRequestHandler();

const logger = new Logging();

// Prepare app
app.prepare()
    .then(() => {
        // Express server instance
        const server = express();
        server.use(express.json());
        server.use(express.urlencoded({ extended: true }));
          if (dev) {
              // use an SSH tunnel for production
              const tunnel = require("tunnel-ssh");
              tunnel(
                  {
                      username: process.env.BASTION_USER,
                      host: process.env.BASTION_HOST,
                      dstHost: process.env.DB_HOST,
                      dstPort: 3306,
                      privateKey: require("fs").readFileSync(process.env.KEY_PAIR_PATH)
                  },
                  function (error, server) {
                      if (error) {
                          logger.error("SSH connection error: " + error);
                      }
  
                      logger.info("SSH connection successful: " + server);
  
                      // Bind the Knex connection
                      // Connect to database
                      Model.knex(
                          Knex({
                              client: 'mysql',
                              connection: {
                                  charset: 'utf8',
                                  database: process.env.DB_NAME,
                                  host: 'localhost',
                                  password: process.env.DB_PWD,
                                  user: process.env.DB_USER,
                              },
                              debug: false,
                              pool: {
                                  max: 1,
                                  min: 1,
                              },
                          })
                      );
                  }
              );
          }
        else {
        Model.knex(
            Knex({
                client: 'mysql',
                connection: {
                    charset: 'utf8',
                    database: process.env.DB_NAME as string,
                    host: process.env.DB_HOST as string,
                    password: process.env.DB_PWD as string,
                    user: process.env.DB_USER as string,
                },
                debug: false,
                pool: {
                    max: 10,
                    min: 1,
                },
            })
        );
         }



        // Config express middlewares
        server.use(helmet());
        server.use(noCache());
        server.use(helmet.noSniff());
        server.use(helmet.dnsPrefetchControl());
        server.use(helmet.ieNoOpen());
        server.disable('x-powered-by');

        // Config logger
        if (process.env.NODE_ENV === 'development') {
            // Configure winston HTTP logger to log all requests in dev mode
            const httpLoger = expressWinston.logger({
                expressFormat: true,
                transports: logger.winston.transports,
                winstonInstance: logger.winston,
            });

            server.use(httpLoger);
        }

        // API routes
        configApiRouting(server);

        // Config routes
        configPageRouting(app, server);

        // Default nextjs route sink
        server.get('*', (req, res) => handle(req, res));

        /**
         * Config express-winston HTTP error logger
         * it MUST be initialized after all routes
         * have been mounted.
         */
        const errorLogger: express.ErrorRequestHandler = expressWinston.errorLogger(
            {
                transports: logger.winston.transports,
                winstonInstance: logger.winston,
            }
        );

        server.use(errorLogger);

        // Uncaught error handling
        server.use((err: any, req: any, res: any, next: any) => {
            res.status(err).jsonp(err)
        })

        server.listen(port, () => {
            logger.info(`> Ready on http://localhost:${port}`);
        });
    })
    .catch((err) =>
        logger.error(
            `> Error starting server @ http://localhost:${port}, ${err}`
        )
    );
