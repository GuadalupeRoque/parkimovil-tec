import { default as express } from 'express';
import { IncomingMessage } from 'http';
import LRUCache from 'lru-cache';
import Server from 'next/dist/next-server/server/next-server';

/**
 * LRU cache config
 */
const SSRCache = new LRUCache({
    max: 100, // 100 items
    maxAge: 1000 * 60 * 60, // 1 hour
});

/**
 * Gets the cache key from the LRU storage
 */
const getCacheKey = (req: any): string => {
    if (req.user) {
        return `${req.url}${req.user.id}`;
    }
    return `${req.url}`;
};

/**
 * Renders and stores in cache a page.
 */
async function renderCache(
    app: Server,
    req: IncomingMessage,
    res: express.Response,
    pagePath: string,
    queryParams?: any
): Promise<void> {
    const key = getCacheKey(req);

    // If we have a page in the cache, let's serve it
    if (SSRCache.has(key)) {
        res.setHeader('x-cache', 'HIT');
        res.send(SSRCache.get(key));
        return;
    }

    try {
        // If not let's render the page into HTML
        const html = await app.renderToHTML(req, res, pagePath, queryParams);

        // Something is wrong with the request, let's skip the cache
        if (res.statusCode !== 200) {
            res.send(html);
            return;
        }

        // Let's cache this page only in production mode
        if (process.env.NODE_ENV === 'production') {
            SSRCache.set(key, html);
        }

        res.setHeader('x-cache', 'MISS');
        res.send(html);
    } catch (err) {
        app.renderError(err, req, res, pagePath, queryParams);
    }
}

/**
 * Public Routes
 * No authentication required
 *
 * @param server
 * @param app
 */
function publicRoutes(server: Server, app: express.Application): void {
    // Index page
    app.get('/', (req: express.Request, res: express.Response) =>
        res.redirect('/formTec')
    )

    // Login page
    app.get('/formTec', (req: express.Request, res: express.Response) =>
        renderCache(server, req, res, '/formTec')
    )
    /*app.get('/', (req: express.Request, res: express.Response) =>
        renderCache(server, req, res, '/')
    );*/
}

/**
 * Private routes.
 * Authentication required
 *
 * @param server
 * @param app
 */
/*function privateRoutes(server: Server, app: express.Application): void {

}*/

/**
 * Configures page routing
 *
 * @param server
 * @param application
 */
export default function configPageRouting(
    server: Server,
    application: express.Application
): void {
    // Config public routes
    publicRoutes(server, application);

    // Config private routes
    // privateRoutes(server, application);
}
