/**
 * Defines an API Error
 *
 * @author Luis Alberto Gómez Rodríguez (alberto.gomez@cargomovil.com)
 * @version 1.0.0 2018/04/06
 * @since 1.0.0 2018/04/06
 */
export interface IHttpException {
  code: number
  error: string | object
  status: number
}

/**
 * Base APIError class
 *
 * Extends NodeJS Error to provide an abstraction layer, for
 * all the JSON API error responses.
 *
 * @author Luis Alberto Gómez Rodríguez (alberto.gomez@cargomovil.com)
 * @version 1.0.0 2018/04/06
 * @since 1.0.0 2018/04/06
 * @extends Error
 */
export class APIError extends Error {
  /**
   * Creates a new HTTP Error instance
   * @param exception the HttpError to create
   *
   * @returns a new instance of Exception
   */
  public static newHttpException(exception: IHttpException): APIError {
    const ex = new APIError(JSON.stringify(exception.error))
    ex.code = exception.code
    ex.status = exception.status
    ex.error = exception.error

    return ex
  }

  public code: number | undefined
  public error: string | object | undefined
  public status: number | undefined

  /**
   * Constructor
   *
   * @param {string} message - The internal error message
   * @since 0.0.1 2016/11/04
   */
  constructor(message: string) {
    super(message)
  }
}
