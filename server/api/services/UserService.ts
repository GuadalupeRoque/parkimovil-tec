import UserTec from '../data/dao/UserTec'
import UserRepo from '../data/repositories/UserRepo'
// @ts-ignore
import { ExceptionHandler } from '@parkimovil/utils/exceptions'
import * as HttpStatus from 'http-status'
import { sign, verify, importKey } from '@parkimovil/utilities'
import fs from 'fs'

export default class UserService {

    public static async insertUserTec(
        email: string, firstName: string, lastName: string, phoneNumber: number, registration: string, areaCodes: string
    ): Promise<UserTec | undefined> {
        try {
            if (phoneNumber.toString().length > 7) {
                const userProfile = await UserRepo.findPhoneNumberUser(phoneNumber, areaCodes);
                if (!userProfile)
                    throw ExceptionHandler.newHttpException({
                        code: 2011,
                        message: 'user not found.',
                        name: HttpStatus['404_NAME'],
                        statusCode: HttpStatus.NOT_FOUND
                    })
                let rol = 'undefined';
                if (registration.startsWith('A')) { rol = 'student' }
                else if (registration.startsWith('L')) { rol = 'collaborator' }
                else throw ExceptionHandler.newHttpException({
                    code: 2015,
                    message: 'user invalid.',
                    name: HttpStatus['403_NAME'],
                    statusCode: HttpStatus.NOT_FOUND
                })
                const promotion = await UserRepo.findPromotion(rol);

                const userTecFind = await UserRepo.findUserId(userProfile.userId);
                if (userTecFind) throw ExceptionHandler.newHttpException({
                    code: 2012,
                    message: 'user duplicate.',
                    name: HttpStatus['409_NAME'],
                    statusCode: HttpStatus.NOT_FOUND
                })

                const userTec = await UserRepo.insertUserTec(userProfile.userId, email, userProfile.phoneAreaCode, userProfile.phoneNumber, rol, registration, firstName, lastName, promotion.id)

                return userTec;
            }
            else {
                throw ExceptionHandler.newHttpException({
                    code: 1001,
                    message: 'invalid argument.',
                    name: HttpStatus['400_NAME'],
                    statusCode: HttpStatus.NOT_FOUND
                })
            }
        }
        catch (error) {
            throw error;
        }
    }


    public static async verifUserTec(query: any): Promise<any | undefined> {
        try {
            const token = query.token;   
            const privateKey = importKey(fs.readFileSync(process.env.PRIVATE_KEY))
            const publicKey = importKey(fs.readFileSync(process.env.PUBLIC_KEY)) 
            const decoded = verify(token, privateKey, publicKey)
            const userTecFind = await UserRepo.findEmailTec(decoded.email);
            if (userTecFind) throw ExceptionHandler.newHttpException({
                code: 2012,
                message: 'user duplicate.',
                name: HttpStatus['409_NAME'],
                statusCode: HttpStatus.NOT_FOUND
            })

            const userTec = {
                firstName: decoded.firstName,
                lastName: decoded.lastName,
                email: decoded.email,
                registration: decoded.matricula
            }
            return userTec;

        }
        catch (error) {
            throw error;
        }
    }
}