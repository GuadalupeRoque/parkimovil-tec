import { Application } from 'express'
import { APIError } from './APIError'
import UserController from './controllers/UserController'

/**
 * Public app routes, no authentication required.
 *
 * @author Luis Gomez
 * @version 0.0.1 2019/05/18
 * @since 0.0.1 2019/05/18
 */
export const configApiRouting = (express: Application) => {

  express.use('/api/user', UserController.configRoutes())

  // Error handling responses
  express.use((err: any, req: any, res: any, next: any) => {
    if (err instanceof APIError) {
      res.status(err.status).jsonp(err)
    } else {
      const ex = APIError.newHttpException({
        code: 500,
        error: err,
        status: 500
      })
      res.status(500).jsonp(ex)
    }
  })
}

export default configApiRouting;