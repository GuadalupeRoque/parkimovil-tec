import { Model, snakeCaseMappers } from 'objection'

/**
 * @author Guadalupe Roque
 * @version 0.0.1 29/07/2020
 * @since 0.0.1 29/07/2020
 */
export default class UserProfile extends Model {
    public static columnNameMappers = snakeCaseMappers()

    public static tableName = 'SEC_USER_PROFILE'
    public static idColumn = 'id'
    public static modelPaths = [__dirname]

    public static relationMappings = {}

    public id!: number
    public userId!: number
    public facebookId!: string
    public facebookUsername!: string
    public facebookEmail!: string
    public firstName!: string
    public lastName!: string
    public phoneAreaCode!: string
    public phoneNumber!: string 
    public openPayCustomerId!: string
}
