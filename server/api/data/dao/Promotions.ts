import { Model, snakeCaseMappers } from 'objection'

/**
 * @author Guadalupe Roque
 * @version 0.0.1 29/07/2020
 * @since 0.0.1 29/07/2020
 */
export default class Promotions extends Model {
    public static columnNameMappers = snakeCaseMappers()

    public static tableName = 'PKM_PROMOTION_CAT'
    public static idColumn = 'id'
    public static modelPaths = [__dirname]

    public static relationMappings = {}

    public id!: number
    public parkingLotId!: number
    public promotionTypeId !: number
    public promotionCode!: string
    public description!: string
    public promotionValue!: number
    public timeInterval !: number
    public validFrom !: string 
    public validTo !: string 
    public redeemCount!: number
    public maxRedeemCount !: number
    public status !: number
    public lastChangeIp !: string 
    public lastModified!: string
    public lastChangeUserId !: number
}
