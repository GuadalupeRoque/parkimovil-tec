import { Model, snakeCaseMappers } from 'objection'

/**
 * @author Guadalupe Roque
 * @version 0.0.1 29/07/2020
 * @since 0.0.1 29/07/2020
 */
export default class ParkingLot extends Model {
    public static columnNameMappers = snakeCaseMappers()

    public static tableName = 'PKM_PARKING_LOT_CAT'
    public static idColumn = 'id'
    public static modelPaths = [__dirname]

    public static relationMappings = {}
    
    public id!: number
   
}
