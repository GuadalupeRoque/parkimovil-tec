import { Model, snakeCaseMappers } from 'objection'

/**
 * @author Guadalupe Roque
 * @version 0.0.1 29/07/2020
 * @since 0.0.1 29/07/2020
 */
export default class User extends Model {
    public static columnNameMappers = snakeCaseMappers()

    public static tableName = 'SEC_USER'
    public static idColumn = 'id'
    public static modelPaths = [__dirname]

    public static relationMappings = {}

    public id!: number
    public username!: string
    public secret!: string
    public userType!: number
    public status!: number
    public lastLogin!: string
    public lastChangeIp!: string
    public lastChangeTimestamp!: string
    public lastChangeUserId!: number
}
