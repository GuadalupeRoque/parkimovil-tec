import { Model, snakeCaseMappers } from 'objection'

/**
 * @author Guadalupe Roque
 * @version 0.0.1 29/07/2020
 * @since 0.0.1 29/07/2020
 */
export default class UserTec extends Model {
    public static columnNameMappers = snakeCaseMappers()

    public static tableName = 'sec_user_tec'
    public static idColumn = 'id'
    public static modelPaths = [__dirname]

    public static relationMappings = {}

    public id!: number
    public userId!: number
    public phoneAreaCode!: string
    public phoneNumber!: string
    public emailTec!: string
    public rolTec!: string
    public createdAt!: string
    public status!: string
    public enrollment!: string
    public firstName!: string
    public lastName!: string
    public promotionId!: number


}
