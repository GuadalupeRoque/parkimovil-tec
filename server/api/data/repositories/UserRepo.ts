import { Transaction, transaction } from 'objection'
import moment from 'moment';
import UserProfile from '../dao/UserProfile';
import UserTec from '../dao/UserTec'
import Promotions from '../dao/Promotions'
import ParkingLot from '../dao/ParkingLot'

export function findUserId(userId: number): Promise<UserTec | undefined> {
    const query = UserTec.query()
        .first()
        .select()
        .where('user_id', userId).execute();
    return query
}

export function findEmailTec(email: string): Promise<UserTec | undefined> {
    const query = UserTec.query()
        .first()
        .select()
        .where('email_tec', email).execute();
    return query
}

export function finParkingLotId(name: string): Promise<ParkingLot | undefined> {
    const query = ParkingLot.query()
        .first()
        .select(`${ParkingLot.tableName}.id`)
        .where('parkingLotName', name).execute();
    return query
}

export async function findPromotion(rol: string): Promise<Promotions | undefined> {    
    const parkingLotId = await finParkingLotId('TEC Santa Fe');
    const query = Promotions.query()
        .first()
        .select(`${Promotions.tableName}.id`)
        .where('promotionCode', rol).andWhere('parkingLotId', parkingLotId.id).execute();
    return query
}

export function findPhoneNumberUser(phoneNumber: number, areaCodes: string): Promise<UserProfile | undefined> {
    const query = UserProfile.query()
        .first()
        .select(`${UserProfile.tableName}.id`,
            `${UserProfile.tableName}.userId`,
            `${UserProfile.tableName}.phoneAreaCode`,
            `${UserProfile.tableName}.phoneNumber`
        )
        .where('phoneNumber', phoneNumber).andWhere('phoneAreaCode', areaCodes).execute();
    return query
}

export async function insertUserTec
    (userId: number, email: string, phoneAreaCode: string, phoneNumber: string, rol: string, registration: string, firstName: string,lastName: string, promotion: number):
    Promise<UserTec | undefined> {
    let trx: Transaction = null;
    try {
        trx = await transaction.start(UserTec.knex());
        const created = await UserTec.query(trx).insert({
            userId: userId,
            phoneAreaCode: phoneAreaCode,
            phoneNumber: phoneNumber,
            emailTec: email,
            rolTec: rol,
            firstName: firstName,
            lastName: lastName,
            promotionId: promotion,
            createdAt: moment().utc().format('YYYY-MM-DD HH:mm:ss'),
            enrollment: registration,
            status: 'active'
        });

        await trx.commit();
        return created;
    }
    catch (error) {
        if (trx !== null) {
            await trx.rollback();
        }
        throw error;
    }

}

export default {
    findUserId,
    findPhoneNumberUser,
    insertUserTec,
    findPromotion,
    findEmailTec
}