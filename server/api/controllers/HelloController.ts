import { Request, Response, Router } from 'express';
import { validationResult, body } from 'express-validator';

export default class HelloController {

    public static configRoutes = (): Router => {
        const router = Router();

        // Config routes below
        router.get(
            '/say-hello',
            (req: Request, res: Response) => {
                const errors = validationResult(req);
                if (!errors.isEmpty()) {
                    return res.status(402).jsonp(errors.mapped());
                }

                return res.status(200).jsonp({ message: 'Hello World!' });
            }
        );

        router.post(
            '/say-hello',
            [
                body('firstName').isString().isLength({ min: 3, max: 50 }),
                body('lastName').isString().isLength({ min: 3, max: 50 }),
            ],
            (req: Request, res: Response) => {
                const errors = validationResult(req);
                if (!errors.isEmpty()) {
                    return res.status(402).jsonp(errors.mapped());
                }

                return res
                    .status(200)
                    .jsonp({
                        message: `Hello ${req.body.firstName} ${req.body.lastName}!`,
                    });
            }
        );

        return router;
    }
}
