import { NextFunction, Request, Response, Router } from 'express'
import { body, validationResult, query } from 'express-validator'
import UserService from '../services/UserService'
import { route } from 'next/dist/next-server/server/router'


export default class UserController {

    public static configRoutes = (): Router => {
        const router = Router()

        router.post('/userTec', [
            body('email')
                .isEmail(),
            body('firstName')
                .isString(),
            body('lastName')
                .isString(),
            body('phoneNumber')
                .isInt()
                .toInt(10),
            body('registration')
                .isString(),
            body('areaCodes')
                .isString()
        ], (req: Request, res: Response, next: NextFunction) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(402).jsonp(errors.mapped());
            }
            return UserService.insertUserTec(req.body.email, req.body.firstName, req.body.lastName, req.body.phoneNumber, req.body.registration, req.body.areaCodes)
                .then(result => res.status(200).jsonp(result))
                .catch(error => next(error))
        })

        router.get('/verifToken', [
            query('token')
                .isString(),
        ], (req: Request, res: Response, next: NextFunction) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(402).jsonp(errors.mapped());
            }
            return UserService.verifUserTec(req.query)
                .then(result => res.status(200).jsonp(result))
                .catch(error => next(error))
        })
        return router
    }

}