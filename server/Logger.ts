import { createLogger, format, Logger, transports } from 'winston'

/**
 * Creates and configures a Winston Logger instance.
 *
 * @class Logger
 *
 * @author Luis Gomez
 * @version 1.0.0 2019/07/05
 * @since 1.0.0 2019/07/05
 */
export default class Logging {
  // winston logger
  private logger: Logger
  get winston(): Logger {
    return this.logger
  }
  set winston(logger: Logger) {
    this.logger = logger
  }

  /**
   * Constructor
   *
   * @since 1.0.0 2019/07/05
   */
  constructor() {
    this.logger = createLogger({
      transports: [
        new transports.Console({
          format: format.colorize({
            all: true,
            colors: {
              debug: 'green',
              error: 'red',
              info: 'blue',
              warn: 'yellow'
            }
          })
        })
      ]
    })
  }

  /**
   * Logs an error message.
   *
   * @param message The message to log
   *
   * @since 1.0.0 2019/07/05
   */
  public error(message: string): void {
    this.logger.error(message)
  }

  /**
   * Logs an info message.
   *
   * @param message The message to log
   *
   * @since 1.0.0 2019/07/05
   */
  public info(message: string): void {
    this.logger.info(message)
  }
}