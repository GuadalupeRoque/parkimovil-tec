module.exports = {
  theme: {
    colors: {
      orange: '#F56E20',
      white: '#FFFFFF',
      red: {
        '100': '#FFF5F5',
        '200': '#FED7D7',
        '300': '#FEB2B2',
        '400': '#FC8181',
        '500': '#F56565',
        '600': '#E53E3E',
        '700': '#C53030',
        '800': '#9B2C2C',
        '900': '#742A2A',
      },
      gray: {
        '100': '#f5f5f5',
        '200': '#eeeeee',
        '300': '#e0e0e0',
        '400': '#bdbdbd',
        '500': '#9e9e9e',
        '600': '#757575',
        '700': '#616161',
        '800': '#424242',
        '900': '#212121',
      },
      blue: '#323C78'
    },
    screens: {
      'sm': '640px',
      // => @media (min-width: 640px) { ... }

      'md': '768px',
      // => @media (min-width: 768px) { ... }

      'lg': '1024px',
      // => @media (min-width: 1024px) { ... }

      'xl': '1280px',
      // => @media (min-width: 1280px) { ... }
    },
    extend: {},
  },
  variants: {},
  plugins: [],
};
